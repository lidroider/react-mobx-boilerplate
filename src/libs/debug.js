import { isProduction } from "utils/constants";

window.debug = {
    log: function () {
        if (!isProduction) {
            console.log.apply(null, arguments);
        }
    },
    error: function () {
        if (!isProduction) {
            console.error.apply(null, arguments);
        }
    },
    trace: function () {
        if (!isProduction) {
            console.trace.apply(null, arguments);
        }
    }
};
