import {store} from "rfx-core";
import NewsState from "./NewsState";

export default store.setup({
    newsState: NewsState,
});
