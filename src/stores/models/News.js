import AModel from "./AModel";
import {observable} from "mobx";

export default class News extends AModel {
    @observable     title;
    @observable     excerpt;
    @observable     image_url;
    @observable     image_urls;
    @observable     content;
    @observable     tags;
    @observable     author;
    @observable     author_id;
    @observable     last_editor;
    @observable     product_ref;
    @observable     active;
}
