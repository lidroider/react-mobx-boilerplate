export default class AModel {
    constructor(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                this[key] = obj[key];
            }
        }
    }
}