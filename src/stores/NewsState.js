import AState from "./AState";
import {action, observable} from "mobx";
import httpClient from "../services/HttpClient";
import News from "./models/News";

export default class NewsState extends AState {
    @observable newsList;
    @observable formNews;

    constructor() {
        super();
        this.loadNews();

        this._formList = ["formNews"];
        this.bootstrap();
    }

    @action setNewsList(newsList) {
        this.newsList = newsList;
    }

    @action.bound initFormNews(news = {}) {
        this.formNews = new News(news);
    }

    async loadNews() {
        try {
            let newsList = [new News({
                id: 1,
                title: "Tiêu đề",
                excerpt: "Tổng quan",
                content: "Nội dung",
            })];
            this.setNewsList(newsList.map(newsItem => new News(newsItem)));
        } catch (e) {
            debug.error(e);
        }
    }

    getNews(id) {
        debug.log(this.newsList.toJS())
        return this.newsList.toJS().find((item) => item.id == id);
    }

    addNews(news) {
        news.id = 2;
        this.newsList.unshift(news);
    }

    removeNews(id) {
        for (let i=0; i<this.newsList.length; i++) {
            if (this.newsList[i].id == id) {
                this.newsList.splice(i, 1);
                break;
            }
        }
    }

    replaceNews(news) {
        debug.log('replaceNews', news);
        for(let i=0; i<this.newsList.length; i++) {
            if (this.newsList[i].id == news.id) {
                this.newsList[i] = news;
                break;
            }
        }
    }

    // getArticles(query) {
    //     return httpClient.get("/api/v1/news", query).then(res => res.news);
    // }
    //
    // getNewsById(id) {
    //     return httpClient.get(`/api/v1/news/${id}`).then(res => res.news);
    // }
    //
    // createNews(news) {
    //     return httpClient.post(`/api/v1/news`, news).then(res => res.news);
    // }
    //
    // deleteNews(id) {
    //     return httpClient.delete(`/api/v1/news/${id}`).then(res => res.news);
    // }
    //
    // updateNews(news) {
    //     if (!news) {
    //         throw new Error('Không tìm thấy id');
    //     }
    //     return httpClient.put(`/api/v1/news/${news.id}`, news).then(res => res.news);
    // }


}
