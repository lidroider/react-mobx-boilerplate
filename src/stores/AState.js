import {action, observable} from "mobx";

export default class AState {

    _formList = [];
    constructor() {

    }

    bootstrap() {
        for (let form of this._formList) {
            this.resetForm(form);
        }
    }

    @action.bound resetForm(name) {
        if (!this[name]) {
            this[name] = observable({});
        }
    }

    @action.bound updateField(formName, name, value) {
        debug.log(formName, name, value);
        if (!this[formName]) {
            throw new Error(`[UserState]Field not found: ${formName}`);
        }
        this[formName][name] = value;
    }
}
