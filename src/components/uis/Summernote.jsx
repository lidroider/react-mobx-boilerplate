import React from 'react';

export default class Summernote extends React.Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.onChange = this.onChange.bind(this);
        this.setText = this.setText.bind(this);
        this.getText = this.getText.bind(this);
    }

    onChange(code) {
        this.props.onChange && this.props.onChange(code);
    }

    componentDidMount() {
        const node = this.myRef.current;
        let that = this;
        $(node).summernote({
            placeholder: '',
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['fontname', 'fontsize', 'color', 'bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['para', ['style', 'ol', 'ul', 'paragraph', 'height']],
                ['insert', ['picture', 'link', 'video', 'table', 'hr']],
                ['misc', ['undo', 'redo', 'codeview', 'fullscreen']]
            ],
            callbacks: {
                onChange: function (content, $editable) {
                    debug.log('on change');
                    that.props.onChange && that.props.onChange(content);
                }
            },
            tabsize: 2,
            height: 400,
            lang: 'vi-VI'
        });
        $(node).summernote('code', this.props && this.props.value);
    }

    setText(text) {
        const node = this.myRef.current;
        $(node).summernote('code', text);
    }

    getText() {
        const node = this.myRef.current;
        let code = $(node).summernote('code');
        return code;
    }

    componentWillUnmount() {
        const node = this.myRef.current;
        $(node).summernote('destroy');
    }

    render() {
        return <div ref={this.myRef}/>;
    }
}
