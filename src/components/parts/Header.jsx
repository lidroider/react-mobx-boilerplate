import {Component} from "react";
import React from "react";

export default class Header extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="header">
                <div className="d-inline-flex align-items-end">
                    <button className='btn btn-lg btn-link' style={{marginBottom: 0}}>
                        <i className="fa fa-plus-circle"></i>
                    </button>
                    <button className='btn btn-lg btn-link' style={{marginBottom: 0}}>
                        <i className="fas fa-hand-holding-usd"></i>
                    </button>
                </div>
                <div style={{marginLeft: '20%'}} className="d-inline-flex align-items-center">
                    <input type="text" className='header-input-search' placeholder=''/>
                </div>

            </div>
        )
    }
}
