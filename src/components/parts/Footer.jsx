import React from "react";
import {Component} from "react";

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer">
                <div className="container-fluid">
                    <nav className="float-left">
                        <ul>
                            <li>
                                <a href="#">
                                    React + Mobx boilerplate
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </footer>
        );
    }
}
