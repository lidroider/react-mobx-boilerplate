import React, {Component} from "react";

export default class CardBody extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card-body">
                {this.props.children}
            </div>
        )
    }
}