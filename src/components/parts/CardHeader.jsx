import React, {Component} from "react";

export default class CardHeader extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="card-header card-header-primary">
                {this.props.children}
            </div>
        )
    }
}