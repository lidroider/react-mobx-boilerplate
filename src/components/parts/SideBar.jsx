import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {Link, withRouter} from "react-router-dom";

@withRouter
@observer
export default class SideBar extends Component {
    getNavLinkClass = (path, exact) => {
        if (exact) {
            return this.props.location.pathname === path ? 'active' : '';
        } else {
            return this.props.location.pathname.indexOf(path) > -1 ? 'active' : '';
        }
    };

    logout() {
        let {history} = this.props;
        try {
            history.push('/login')
        } catch (e) {
            $.notify({
                message: e.message
            }, {
                type: "danger",
                timer: 3000,
            });
        }
    }

    render() {
        const {menus} = this.props;
        let menusItems = menus.map((menu, i) => (
            <li key={i} className={this.getNavLinkClass(menu.link)}>
                <Link to={menu.link}>
                    <i className="material-icons">{menu.icon}</i>
                    <p>{menu.title}</p>
                </Link>
            </li>
        ));
        return (
            <div className="sidebar admin-sidebar" data-color="white" data-active-color="primary"
                 data-image="../assets/img/sidebar-1.jpg">
                <div className="logo">
                    <Link to="/" className="simple-text logo-normal">
                        React + MobX boilerplate
                    </Link>
                </div>
                <div className="sidebar-wrapper admin-sidebar-wrapper">
                    <ul className="nav">
                        {menusItems}
                    </ul>
                </div>
                <div className="admin-sidebar-footer">
                    <button className="btn btn-warning btn-sm" onClick={() => this.logout()}>
                        Đăng xuất
                    </button>
                </div>
            </div>
        );
    }
}
