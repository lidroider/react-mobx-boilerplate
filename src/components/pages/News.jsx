import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {inject, observer} from "mobx-react";
import NewsList from "./news/NewsList";
import NewsDetail from "./news/NewsDetail";

@inject("newsState")
@observer
export default class News extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let {match} = this.props;
    return (<React.Fragment>
      <div className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-12">
              <Switch>
                <Route exact path={match.url} component={NewsList}/>
                <Route path={match.url + "/create"} component={NewsDetail}/>
                <Route path={match.url + "/:id"} component={NewsDetail}/>
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>)
  }
}
