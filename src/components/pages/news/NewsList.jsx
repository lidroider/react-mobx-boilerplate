import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import Card from "../../parts/Card";
import CardHeader from "../../parts/CardHeader";
import CardBody from "../../parts/CardBody";
import NewsDetail from "./NewsDetail";

@inject("newsState")
@observer
export default class NewsList extends Component {
    constructor(props) {
        super(props);
        this.confirmModalref = React.createRef();
    }

    confirmDeleteNews(news) {
        let confirmModal = this.confirmModalref.current;
        confirmModal.show && confirmModal.show({object: news,});
    }

    async deleteNews(news) {
        let {newsState} = this.props;
        let {deleteNews} = newsState;
        try {
            // await deleteNews(news.id);
            // gọi api
            newsState.removeNews(news.id);
            $.notify({
                message: 'Xóa tin tức thành công'
            }, {
                type: "success",
                timer: 3000
            });
        } catch (e) {
            $.notify({
                message: e.message
            }, {
                type: "danger",
                timer: 3000
            });
        }
    }

    render() {
        let {history, match} = this.props;
        let {newsList} = this.props.newsState;
        let newItems = newsList && newsList.map(newItem => (<tr key={newItem.id}>
            <td>{newItem.id}</td>
            <td>{newItem.title}</td>
            <td>{newItem.excerpt}</td>
            <td>
                <i className="material-icons pointer" onClick={() => history.push(`${match.url}/${newItem.id}`)}>
                    edit
                </i>
                <i className="material-icons pointer" onClick={() => this.deleteNews(newItem)}>
                    delete_forever
                </i>
            </td>
        </tr>));
        return (
            <React.Fragment>
                <Card>
                    <CardHeader>
                        <span className='font-huge card-title inline-block-middle mr-3'>Tin tức</span>
                        <a onClick={() => history.push(`${match.url}/create`)}
                           className="btn btn-sm btn-primary inline-middle btn-align-middle">
                            <i className="font-normalsize material-icons">add</i>
                            <span>Thêm tin tức</span>
                        </a>
                    </CardHeader>
                    <CardBody>
                        <div className="row">
                            <div className="col-md-12 float-right">

                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead className="text-primary">
                                        <tr>
                                            <th width="5%">ID</th>
                                            <th width="20%">Tiêu đề</th>
                                            <th width="50%">Tổng quan</th>
                                            <th width="*">#</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {newItems}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </CardBody>
                </Card>
            </React.Fragment>
        );
    }
}
