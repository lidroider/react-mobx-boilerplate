import React, {Component} from "react";
import {inject, observer} from "mobx-react";
import {observable} from "mobx";
import Summernote from '../../uis/Summernote';
import Card from "../../parts/Card";
import CardHeader from "../../parts/CardHeader";
import CardBody from "../../parts/CardBody";
import notify from "../../../services/Notify";
import util from "../../../services/Util";

@inject("newsState")
@observer
export default class NewsDetail extends Component {
    constructor(props) {
        super(props);
        this.onSumitForm = this.onSumitForm.bind(this);
        this.summernoteRef = React.createRef();
    }

    validate() {
        let {newsState} = this.props;
        let {formNews, updateField} = newsState;

        if (!formNews.title || formNews.title.length == 0) {
            $.notify({
                message: 'Vui lòng nhập tiêu đề tin tức'
            }, {
                type: "danger",
                timer: 3000
            });
            return false;
        }

        if (formNews.title.length > 80) {
            $.notify({
                message: 'Tiêu đề tin tức không quá 80 kí tự'
            }, {
                type: "danger",
                timer: 3000
            });
            return false;
        }

        if (!formNews.excerpt || formNews.excerpt.length == 0) {
            $.notify({
                message: 'Vui lòng nhập tổng quát tin tức'
            }, {
                type: "danger",
                timer: 3000
            });
            return false;
        }

        if (formNews.excerpt.length > 200) {
            $.notify({
                message: 'Tổng quát tin tức không quá 200 kí tự'
            }, {
                type: "danger",
                timer: 3000
            });
            return false;
        }

        return true;
    }

    async onSumitForm(event) {
        event.preventDefault();
        let {newsState, match, history} = this.props;
        let {updateNews, createNews, formNews} = newsState;
        let id = match && match.params && match.params.id;

        if (!this.validate())
            return;

        try {
            if (!id) {
                // let news = await createNews(formNews);
                // newsState.addNews(news);
                // truyền api ở đây

                newsState.addNews(formNews);

                history.push('/news');
                $.notify({
                    message: 'Tạo tin tức thành công'
                }, {
                    type: "success",
                    timer: 3000
                });
            } else {
                // let news = await updateNews(formNews);
                // newsState.replaceNews(news);
                // truyền api ở đây

                newsState.replaceNews(formNews);

                history.push('/news');
                $.notify({
                    message: 'Cập nhật tin tức thành công'
                }, {
                    type: "success",
                    timer: 3000
                });
                history.push('/news');
            }
        } catch (e) {
            $.notify({
                message: e.message
            }, {
                type: "danger",
                timer: 3000
            });
        }
    }

    async loadNewsDetail() {
        let {newsState} = this.props;
        let {initFormNews, getNewsById, getNews} = newsState;
        let {match} = this.props;
        let id = match && match.params && match.params.id;
        debug.log('loadNewsDetail', id);
        if (!id) {
            return initFormNews();
        }
        try {
            // let news = await getNewsById(id);
            // gọi api ở đây

            debug.log('loadNewsDetail', 'a');
            let news = newsState.getNews(id);
            debug.log('loadNewsDetail', news);

            initFormNews(news);
            this.summernoteRef.current.setText(news.content);
        } catch (e) {
            notify.error(e.message);
        }
    }

    componentDidMount() {
        this.loadNewsDetail();
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    render() {
        let {formNews, updateField} = this.props.newsState;
        let {match} = this.props;
        let id = match && match.params && match.params.id;

        return (<React.Fragment>
            <Card>
                <CardHeader>
                    <h4 className='card-title'>Tạo tin tức</h4>
                </CardHeader>
                <CardBody>
                    <form onSubmit={this.onSumitForm}>
                        <div className="form-group">
                            <label htmlFor="title">Tiêu đề</label>
                            <input className="form-control" placeholder="Tiêu đề"
                                   onChange={(event) => updateField("formNews", "title", event.target.value)}
                                   value={formNews && formNews.title || ''}/>
                        </div>
                        <div className="form-group">
                            <label htmlFor="excerpt">Tổng quát</label>
                            <input className="form-control" placeholder="Tổng quát"
                                   onChange={(event) => updateField("formNews", "excerpt", event.target.value)}
                                   value={formNews && formNews.excerpt || ''}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="content">Nội dung</label>
                            <Summernote ref={this.summernoteRef}
                                        onChange={(value) => updateField("formNews", "content", value)}/>
                        </div>
                        <div className="pull-right">
                            <button type="submit" className="btn btn-primary btn-sm">{
                                (id && id > 0)
                                    ? 'Cập nhật tin tức'
                                    : 'Tạo tin tức'
                            }</button>
                        </div>
                    </form>
                </CardBody>
            </Card>
        </React.Fragment>)
    }
}
