import React from 'react';
import {inject, observer} from 'mobx-react';

@observer
export default class Login extends React.Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
    }

    async login(e) {
        e.preventDefault();
        let {history} = this.props;
        try {
            history.push('/');
        } catch (e) {
            $.notify({
                message: e.message
            }, {
                type: "danger",
                timer: 3000,
            });
        }

    }

    render() {
        return (
            <React.Fragment>
                <div className="wrapper wrapper-full-page ">
                    <div className="section-image full-page" filter-color="black">
                        <div className="content">
                            <div className="container">
                                <div className="col-lg-4 col-md-6 ml-auto mr-auto">
                                    <form className="form" onSubmit={(e) => this.login(e)}>
                                        <div className="card card-login">
                                            <div className="card-header ">
                                                <div className="card-header ">
                                                    <h3 className="header text-center">Đăng nhập</h3>
                                                </div>
                                            </div>
                                            <div className="card-body ">
                                                <div className="input-group">
                                                    <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="nc-icon nc-single-02"></i>
                                            </span>
                                                    </div>
                                                    <input type="text" className="form-control" placeholder="Email"
                                                    />
                                                </div>
                                                <div className="input-group">
                                                    <div className="input-group-prepend">
                                            <span className="input-group-text">
                                                <i className="nc-icon nc-key-25"></i>
                                            </span>
                                                    </div>
                                                    <input type="password" placeholder="Password"
                                                           className="form-control"
                                                    />
                                                </div>
                                            </div>
                                            <div className="card-footer ">
                                                <input type="submit"
                                                       className="btn btn-warning btn-round btn-block mb-3"
                                                       value="Đăng nhập"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}
