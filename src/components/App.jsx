import React, {Component} from "react";
import {Route, Switch, withRouter, Redirect} from "react-router-dom";
import Footer from "./parts/Footer.jsx";
import SideBar from "./parts/SideBar";
import News from "./pages/News";
import Login from "./pages/Login";

@withRouter
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    const menus = [
      {
        title: "Tin Tức",
        icon: "chrome_reader_mode",
        link: "/news",
        exact: false,
        component: News
      }
    ];

    const routes = menus.map((menu, i) => (<Route key={i} exact={menu.exact} path={menu.link} component={menu.component}/>));

    let {match} = this.props;
    let checkLogin = true;
    return (<React.Fragment>
      <Switch>
        <Route exact={true} path="/login" component={Login}/>
        <Route render={() => {
            if (!checkLogin) {
              return (<Redirect from={match.path} to='/login'/>);
            } else {
              return (<div className="wrapper font-normalsize">
                <SideBar menus={menus}/>
                <div className="main-panel">
                  <nav className="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
                    <div className="container-fluid">
                      <div className="navbar-wrapper">
                        <a className="navbar-brand"></a>
                      </div>
                    </div>
                  </nav>
                  <Switch>
                    {routes}
                  </Switch>
                  <Footer/>
                </div>
              </div>);
            }
          }}/>
      </Switch>
    </React.Fragment>);
  }
}
