const isProduction = process.env.NODE_ENV === "production";
const appId = process.env.appId;
export { isProduction, appId };
