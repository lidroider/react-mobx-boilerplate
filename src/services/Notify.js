const notify = {};

let methods = [{
    method: 'info',
    type: 'info'
  },
  {
    method: 'success',
    type: 'success'
  },
  {
    method: 'warning',
    type: 'warning'
  },
  {
    method: 'error',
    type: 'danger'
  }
]
methods.forEach((define) => {
  notify[define.method] = function(message, title, opts = {}) {
    $.notify({
      title: title,
      message: message,

    }, {
      type: define.type,
      onShow: opts.onShow,
      onShown: opts.onShown,
      onClose: opts.onClose,
      onClosed: opts.onClosed
    })
  }
})

export default notify;
