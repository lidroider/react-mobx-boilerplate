import axios from "axios";
import {isProduction} from "utils/constants";

export class HttpClient {
    createHeader(token) {
        const jwt = "truyen token vao day"; //token || storage.token || "";
        return {
            "Authorization": jwt,
            "Content-Type": "application/json"
        }
    }

    createOption(opts = {}) {
        return {
            headers: opts.headers || this.createHeader(),
            ...opts
        };
    }

    get(link, query) {
        const options = this.createOption();
        options.params = query;
        return this.getWithOptions(link, options);
    }

    post(link, data) {
        const options = this.createOption();
        return this.postWithOptions(link, data, options);
    }

    put(link, data) {
        const options = this.createOption();
        return this.putWithOptions(link, data || {}, options);
    }

    delete(link) {
        const options = this.createOption();
        return this.deleteWithOptions(link, options);
    }

    postWithOptions(link, data, options) {
        return axios.post(`${link}`, data, options).then(this._extractData).catch(this.handleError);
    }

    getWithOptions(link, options) {
        return axios.get(`${link}`, options).then(this._extractData).catch(this.handleError);
    }

    putWithOptions(link, data, options) {
        return axios.put(`${link}`, data, options).then(this._extractData).catch(this.handleError);
    }

    deleteWithOptions(link, options) {
        return axios.delete(`${link}`, options).then(this._extractData).catch(this.handleError);
    }

    _extractData(res) {
        if (!isProduction) {
            console.log(res.request.responseURL, res);
        }
        let data = res.data;
        if (data && !data.error) {
            return data;
        }
        throw data.error;
    }

    handleError(error) {
        if (!isProduction) {
            console.log("handleError", error);
        }
        // In a real world app, you might use a remote logging infrastructure
        const errMsg = {
            code: error.code,
            message: error.message
        };
        throw(errMsg);
    }
}

const httpClient = new HttpClient();
export default httpClient;
