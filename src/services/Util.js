import * as moment from 'moment';

export class Util {
  formatDateTime(date) {
    return moment(date).format('HH:mm:SS MM/DD/YYYY');
  }
}

const util = new Util();
export default util;
